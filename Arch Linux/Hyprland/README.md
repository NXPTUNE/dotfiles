# My Dracula theme-based Arch Linux Hyprland rice! 🧛

<img src="https://gitlab.com/NXPTUNE/dotfiles/-/raw/main/Arch%20Linux/Hyprland/assets/screenshot.png" width=auto align="center">

## How to install and use
#### **[Installing Arch Linux 💽](https://wiki.archlinux.org/title/Installation_guide)**

**Important!** In case if you are using wi-fi only, I heavily recommend installing the <code>iwd</code> (for <code>iwctl</code>) and <code>dhcpcd</code> packages and enabling the daemons before the first reboot after Arch Linux installation. Otherwise, you may not be able to use any network after rebooting into your fresh installed system.

```
# pacman -S dhcpcd iwd
```
```
# systemctl enable --now dhcpcd iwd
```

#### **[Installing Hyprland 🔧](https://wiki.hyprland.org/Getting-Started/Installation/)**

#### **[Waybar 📌](https://github.com/Alexays/Waybar)**

#### **[Dracula 🧛](https://draculatheme.com)**
---
### Packages 📦

Install all the following packages for a full experience:

- **bluez** (if using bluetooth)
- **bluez-utils** (if using bluetooth)
- **btop**
- **cava**
- **curl**
- **cmatrix-git**
- **git**
- **grim**
- **hyprpaper-git**
- **man**
- **neofetch**
- **pipewire**
- **pipewire-audio**
- **pipewire-pulse**
- **slurp**
- **starship**
- **waybar-hyprland-git**
- **wget**
- **wofi**
- **yay** (an AUR helper)
- **zsh**
---
Install **yay**:
```
$ sudo pacman -S --needed git base-devel && git clone https://aur.archlinux.org/yay.git && cd yay && makepkg -si
```
---
Install software:
```
$ sudo pacman -S bluez bluez-utils btop curl git grim man neofetch pipewire pipewire-audio pipewire-pulse slurp starship wget wofi zsh
```
```
$ yay -S cava cmatrix-git hyprpaper-git waybar-hyprland-git
```
After  installing **zsh** you need to install **Oh My Zsh**, **zsh-autosuggestions** and **zsh-syntax-highlighting** plugins, and apply **starship** prompt.
Use the following command to install **Oh My Zsh**:
```
$ sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
```
Then install the plugins:
```
$ git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
```
```
$ git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
```
Copy and overwrite your <code>.zshrc</code> file with mine to apply all your shell settings and enjoy!

---
Install fonts:
- all the **noto-fonts** for browsers
- **fantasque-sans-mono** for the system and waybar
- **font-awesome** for the waybar icons
```
$ sudo pacman -S noto-fonts noto-fonts-cjk noto-fonts-emoji noto-fonts-extra otf-fantasque-sans-mono otf-font-awesome ttf-fantasque-nerd ttf-nerd-fonts-symbols-2048-em ttf-nerd-fonts-symbols-2048-em-mono ttf-nerd-fonts-symbols-common
```

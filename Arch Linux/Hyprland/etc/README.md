### Pacman <code>Color</code> option work demonstration 
---
<img src="https://gitlab.com/NXPTUNE/dotfiles/-/raw/main/Arch%20Linux/Hyprland/assets/pacman.png" width=max align="center">
<img src="https://gitlab.com/NXPTUNE/dotfiles/-/raw/main/Arch%20Linux/Hyprland/assets/pacman_coloured.png" width=max align="center">

### Open <code>/etc/pacman.conf</code> file in your favourite text editor, find the following text block,  uncomment <code>Color</code> by removing <code>#</code> and save:

```
# Misc options
#UseSyslog
#Color
#NoProgressBar
CheckSpace
#VerbosePkgLists
#ParallelDownloads = 5
```
